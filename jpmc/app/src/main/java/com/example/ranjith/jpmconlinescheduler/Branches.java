package com.example.ranjith.jpmconlinescheduler;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.ranjith.jpmconlinescheduler.WorkFlow.Book;
import com.example.ranjith.jpmconlinescheduler.WorkFlow.Generate;
import com.example.ranjith.jpmconlinescheduler.WorkFlow.Token;

import java.util.ArrayList;

/**
 * Created by RANJITH on 13-03-2018.
 */

public class Branches extends android.support.v4.app.Fragment{
    String service,var;
    ProgressDialog progressDialog;
    ListView lv;
    SearchView sv;
    ArrayAdapter<String> adapter;
    ArrayList<String> arrayList;
    private View view;
    private int backscreen=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_book, container, false);
        Toast.makeText(getActivity(),getArguments().getString("service"),Toast.LENGTH_LONG).show();
        arrayList=new ArrayList<String>();
        addItems();

        progressDialog=new ProgressDialog(getActivity());
        selection();
//        Spinner spinner = (Spinner) findViewById(R.id.bspinner);
//        final ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,arrayList);
//        spinner.setAdapter(arrayAdapter);
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                var=arrayAdapter.getItem(i);
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        return view;
    }

    private void selection() {
        lv=(ListView) view.findViewById(R.id.listView1);
        sv=(SearchView) view.findViewById(R.id.searchView);
        sv.setQueryHint("Select Branches");
        sv.clearFocus();
        //sv.onActionViewExpanded();
        sv.setIconified(false);
        adapter=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,arrayList);
        lv.setAdapter(adapter);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String text) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean onQueryTextChange(String text) {

                ArrayList<String> arrayList1=new ArrayList<String>();
                for (String items:arrayList){
                    if (items.toLowerCase().contains(text.toLowerCase()))
                    {
                        arrayList1.add(items);
                    }
                }
                ArrayAdapter<String> adapter1=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,arrayList1);
                lv.setAdapter(adapter1);
                adapter.getFilter().filter(text);
                return true;
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Intent intent=new Intent(getActivity(),Generate.class);
//                intent.putExtra("branch",lv.getItemAtPosition(i).toString());
//                intent.putExtra("service",getArguments().getString("service"));
//                startActivity(intent);
                android.support.v4.app.Fragment fragment;
                fragment= new Token();
                if (fragment!=null){
                    android.support.v4.app.FragmentTransaction ft=getFragmentManager().beginTransaction();
                    backscreen=1;

                    ft.replace(R.id.content_main,fragment);
                    ft.addToBackStack("vd");
                    Bundle bundle = new Bundle();
                    bundle.putString("branch", lv.getItemAtPosition(i).toString());
                    bundle.putString("service", getArguments().getString("service"));
                    fragment.setArguments(bundle);
                    ft.commit();

                }


            }
        });
    }

    private void addItems() {
        arrayList.add("SELECT BRANCH");
        arrayList.add("JNTU");
        arrayList.add("JEEDIMETLA");
        arrayList.add("SECUNDERABAD");
        arrayList.add("UPPAL");
        arrayList.add("KUKATPALLY");
        arrayList.add("BACHUPALLY");
        arrayList.add("AMEERPET");
        arrayList.add("BALANAGAR");

    }


    public void book(View view){

        Intent intent=new Intent(getActivity(),Generate.class);
        intent.putExtra("branch",var);
        intent.putExtra("service",service);
        startActivity(intent);

    }









    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Select Banking Details");
        InputMethodManager inputManager = (InputMethodManager) this
                .getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = this.getActivity().getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

    }


}
