package com.example.ranjith.jpmconlinescheduler.WorkFlow;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ranjith.jpmconlinescheduler.R;
import com.example.ranjith.jpmconlinescheduler.Util.JpmcToast;
import com.example.ranjith.jpmconlinescheduler.Util.Utils;
import com.google.android.gms.auth.api.*;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignIn extends AppCompatActivity {
    private static  final int RC_SIGN_IN=1;
    ProgressDialog progressDialog;
    Button signInButton;
    private GoogleSignInClient mGoogleSignInClient;
    GoogleApiClient googleApiClient;
    private String TAG="Response";
    GoogleSignInOptions gso;
    private FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener authStateListener;
    String email="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.ranjith.jpmconlinescheduler.R.layout.activity_sign_in);
        signInButton=(Button)findViewById(R.id.sign_in_button);
        SharedPreferences shareds = getSharedPreferences("loginData", Context.MODE_PRIVATE);
        Toast.makeText(getApplicationContext(), shareds.getString("user",""),Toast.LENGTH_LONG).show();
        mAuth = FirebaseAuth.getInstance();
        final DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference();
        final com.google.firebase.database.Query query = databaseReference.child("Customer").orderByChild("Email").equalTo("g@gmail.com");
        final SharedPreferences shared = getSharedPreferences("loginData", Context.MODE_PRIVATE);
        authStateListener=new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull final FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser()!=null) {
                    if (shared.getString("visited", "").equals("yes")||(shared.getString("mvisited", "").equals("yes") &&
                            FirebaseAuth.getInstance().getCurrentUser().isEmailVerified())) {
                        startActivity(new Intent(SignIn.this, Servicess.class));
                    }
                }
            }
        };
        gso = new GoogleSignInOptions.Builder(
                GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .requestProfile()
                .build();
        googleApiClient=new GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(getApplicationContext(),"ërror",Toast.LENGTH_LONG).show();

                    }
                }).addApi(Auth.GOOGLE_SIGN_IN_API,gso).build();
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });


    }
    private void signIn() {
        if (Utils.hasActiveInternetConnection(SignIn.this)) {
            progressDialog = Utils.showLoadingDialog(SignIn.this, false);

            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);

        }
        else
        {
            JpmcToast.create(this, R.drawable.ic_error_outline_black_24dp, "No Internet\nPlease try again", Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            final FirebaseUser user = mAuth.getCurrentUser();
                            email=user.getEmail();
                            final DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference();
                            final com.google.firebase.database.Query query = databaseReference.child("Customer").orderByChild("Email").equalTo(user.getEmail());
                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(!dataSnapshot.exists())
                                    {
                                        DatabaseReference databaseReferences = databaseReference.child("Customer").push();
                                        databaseReferences.child("Email").setValue(email);
                                        databaseReferences.child("Name").setValue(user.getDisplayName());
                                        progressDialog.cancel();
                                        SharedPreferences shared = getSharedPreferences("loginData", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = shared.edit();
                                        editor.putString("visited","yes");
                                        editor.putString("user",user.getEmail());
                                        editor.putString("image",user.getPhotoUrl()+"");
                                        Toast.makeText(getApplicationContext(),user.getEmail()+""+user.getPhotoUrl(),Toast.LENGTH_LONG).show();
                                        editor.commit();
                                        startActivity(new Intent(SignIn.this, MainActivity.class));


                                    }
                                    else
                                    {
                                        SharedPreferences shared = getSharedPreferences("loginData", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = shared.edit();
                                        editor.putString("user",user.getEmail());
                                        editor.putString("visited","yes");
                                        editor.putString("image",user.getPhotoUrl()+"");
                                        editor.commit();
                                        Toast.makeText(getApplicationContext(),user.getEmail()+""+user.getDisplayName(),Toast.LENGTH_LONG).show();
                                        startActivity(new Intent(SignIn.this, Servicess.class));
                                    }





                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            //
                           // updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
//                            Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
//                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(authStateListener);
    }
    public void register(View view){
        startActivity(new Intent(getApplicationContext(),Registration.class));
    }
    public void logIn(View view) {

        if (Utils.hasActiveInternetConnection(SignIn.this)) {
            progressDialog = Utils.showLoadingDialog(SignIn.this, false);

            final EditText name, password;
            name = (EditText) findViewById(R.id.name);
            password = (EditText) findViewById(R.id.pwd);
            if (!TextUtils.isEmpty(name.getText().toString())) {
                if (!TextUtils.isEmpty(password.getText().toString())) {
                    mAuth.signInWithEmailAndPassword(name.getText().toString(), password.getText().toString())
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (!task.isSuccessful()) {
                                        progressDialog.cancel();
                                        Toast.makeText(SignIn.this, "Login Failed",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                    else if (task.isSuccessful()) {
                                        if (FirebaseAuth.getInstance().getCurrentUser().isEmailVerified()) {
                                            Toast.makeText(SignIn.this, "Login Success", Toast.LENGTH_LONG).show();
                                            //progressDialog.cancel();
                                            SharedPreferences shared = getSharedPreferences("loginData", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = shared.edit();
                                            editor.putString("user", name.getText().toString());
                                            editor.putString("visited", "yes");
                                            editor.putString("image", "");
                                            editor.commit();
                                            startActivity(new Intent(getApplicationContext(), Servicess.class));
                                        } else {
                                            progressDialog.cancel();
                                            Toast.makeText(SignIn.this, "Please verify your Email", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    // ...
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(SignIn.this, "Invalid Password", Toast.LENGTH_SHORT).show();
                            } else if (e instanceof FirebaseAuthInvalidUserException) {
                                Toast.makeText(SignIn.this, "Incorrect email address", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(SignIn.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                            }

                        }
                    });
                    }
                else {
                    progressDialog.cancel();
                    Toast.makeText(this, "Please Enter Password", Toast.LENGTH_SHORT).show();
                }
              }
            else {
                progressDialog.cancel();
                Toast.makeText(this, "Please Enter Email Id", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            JpmcToast.create(this, R.drawable.ic_error_outline_black_24dp, "No Internet\nPlease try again", Toast.LENGTH_SHORT);
        }
    }
    public void changePwd(View view){
        startActivity(new Intent(getApplicationContext(),Pwdchange.class));

    }


    }



