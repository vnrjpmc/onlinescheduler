package com.example.ranjith.jpmconlinescheduler.WorkFlow;

/**
 * Created by RANJITH on 04-03-2018.
 */
public class Duration {
    public String text;
    public int value;

    public Duration(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
